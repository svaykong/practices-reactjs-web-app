import { 
    GET_ARTICLES, 
    ARTICLE_ERROR, 
    INSERT_ARTICLE,
    GET_ARTICLE,  
    DELETE_ARTICLE, 
    UPDATE_ARTICLE, 
    SET_LOADING } from './actionType';

import Axios from 'axios';

export const getAllArticles = () => async dispatch => {
    try {
        setLoading();
        const res = await Axios.get('http://110.74.194.125:3535/api/articles');
        
        let data = null;
        
        if(res.status === 200) {
            data = res.data.data;
        }
        dispatch({
            type: GET_ARTICLES,
            payload: data
        });
    }
    catch(err) {
        dispatch({
            type: ARTICLE_ERROR,
            payload: err.response
        })
    }
    
};

export const insertArticle = (article) => async dispatch => {
    try {
        setLoading();
        const res = await Axios.post('http://110.74.194.125:3535/api/articles', article, {
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            timeout: 3000,
        })

        let data = null;
        if(res.status === 200) {
            data = res.data;
        }

        dispatch({
            type: INSERT_ARTICLE,
            payload: data
        });
    }
    catch(err) {
        dispatch({
            type: ARTICLE_ERROR,
            payload: err.response
        })
    }
    
};

export const deleteArticle = (id) => async dispatch => {
    try {
        setLoading();

        await Axios.delete(`http://110.74.194.125:3535/api/articles/${id}`, { timeout: 3000 });
        
        dispatch({
            type: DELETE_ARTICLE,
            payload: id
        });
    }
    catch(err) {
        dispatch({
            type: ARTICLE_ERROR,
            payload: err.response.status
        })
    }
    
};

export const getArticleById = (id) => async dispatch => {
    try {
        setLoading();
        const res = await Axios.get(`http://110.74.194.125:3535/api/articles/${id}`, { timeout: 3000 })
        let data = null;
        if(res.status === 200) {
            data = res.data.data;
        }

        dispatch({
            type: GET_ARTICLE,
            payload: data
        });
    }
    catch(err) {
        dispatch({
            type: ARTICLE_ERROR,
            payload: err.response
        })
    }
    
};

export const updateArticle = (article) => async dispatch => {
    try {
        setLoading();
    
        const res = await Axios.patch(`http://110.74.194.125:3535/api/articles/${article._id}`, article, {
            data: JSON.stringify(article),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            timeout: 3000,
        })

        let data = null;

        if(res.status === 200) {
            data = res.data;
        }

        dispatch({
            type: UPDATE_ARTICLE,
            payload: data
        });
    }
    catch(err) {
        dispatch({
            type: ARTICLE_ERROR,
            payload: err.response
        })
    }
    
};

//--- set loading to true
export const setLoading = () => {
    return {
        type: SET_LOADING
    }
}
