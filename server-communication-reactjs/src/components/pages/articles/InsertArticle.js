import React, { Component } from 'react'
import { Form, Button, Container, Row, Col, Image } from "react-bootstrap";
import { connect } from "react-redux";
import { insertArticle } from "../../../redux/actions/articleActions";
import { getAllCategories } from '../../../redux/actions/categoryActions';
import Axios from 'axios';

export class InsertArticle extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            title: "",
            description: "",
            category: "",
            categoryId: "",
            image: "",
            imagePreviewUrl: "",
            categories: null,
            titleError: "",
            descriptionError: "",
        }
    }
    
    componentDidMount() {
        getAllCategories((res) => {
            if(res.status === 200) {
                this.setState({ loading: false, categories: res.data.data })
            }
        });
    }

    onChange = (event) => {
        event.preventDefault();
        const name = event.target.name;
        const value = event.target.value;
        if(event.target.type === "radio") {
            this.setState({ categoryId: event.target.id });
        }
        if(event.target.type === "file") {
            let reader = new FileReader();
            let file = event.target.files[0];
            reader.onloadend = () => {
                this.setState({ 
                    image: file,
                    imagePreviewUrl: reader.result
                });
            }
            reader.readAsDataURL(file);
        }
        this.setState({ [name]: value });
    }

    validate = () => {
        let titleError = "";
        let descriptionError = "";
        if(this.state.title === "") {
            titleError = "Please input title!";
        }
        if(this.state.description === "") {
            descriptionError = "Please input description!";
        }
        if(titleError || descriptionError) {
            this.setState({ titleError, descriptionError });
            return false;
        }
        return true;
    }

    onSubmit = (event) => {
        event.preventDefault();
        const isValid = this.validate();
        if(isValid) {
            const newArticle = {}; 
            newArticle.title = this.state.title;
            newArticle.description = this.state.description;
            newArticle.category = {
                "_id": this.state.categoryId,
            };

            let fd = new FormData();
            console.log(this.state.image)
            fd.append('image', this.state.image, this.state.image.name);
        
            this.setState({ loading: true });
            // post image
            Axios.post("http://110.74.194.125:3535/api/images", fd, { 
                headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }})
                .then(res => {
                    if(res.status === 200) {
                        newArticle.image = res.data.url;
                        this.props.insertArticle(newArticle);
                        this.setState({ 
                            loading: false, 
                            title: '', 
                            description: '', 
                            category: '', 
                            image: "", 
                            imagePreviewUrl: "",
                            titleError: "",
                            descriptionError: "",   
                        });
                        alert("Add article successfully!");
                        window.history.back();
                    }
                })
                .catch(error => console.log(error.response));
        }
    }

    render() {
        const { loading, title, description, category, imagePreviewUrl, categories, titleError, descriptionError } = this.state;
        if(loading || categories === null) {
            return <h1>Loading...</h1>;
        }
        return (
            <div>    
            <Container className="py-3">
                <h1>Add Article</h1>
                <div>
                    <Row>
                        <Col md={7}>
                        <Form onSubmit={this.onSubmit}>
                            <Form.Group>
                                <Form.Label>Title</Form.Label>
                                <Form.Control type="text" 
                                    placeholder="Input title" 
                                    name="title" 
                                    onChange={this.onChange}
                                    value={title}/>
                                <h5 className="text-danger">{ titleError ? titleError : ""}</h5>
                            </Form.Group>
                            <Container>
                                <Row>
                                    <div className="form-group">
                                        <Form.Label className="mr-3">Category: </Form.Label>
                                        { categories.map(c => 
                                            <div className="form-check form-check-inline" key={c._id}>
                                            <input
                                                type="radio"
                                                className="form-check-input"
                                                name="category"
                                                value={c.name}
                                                id={c._id}
                                                checked={category === c.name}
                                                onChange={this.onChange}
                                            />
                                            <label className="form-check-label">{c.name}</label>
                                        </div>
                                        )}
                                    </div>         
                                </Row>
                            </Container>
                            <Form.Group>
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" 
                                    placeholder="Input description" 
                                    name="description" 
                                    onChange={this.onChange}
                                    value={description}/>
                                <h5 className="text-danger">{ descriptionError ? descriptionError : ""}</h5>
                            </Form.Group>
                            <Form.File 
                                type="file"
                                id="custom-file"
                                label="Custom file input"
                                name="image"
                                custom
                                className="mb-3"
                                onChange={this.onChange}
                            />
                            <Button variant="primary" type="submit">Submit</Button>
                        </Form>
                        </Col>
                        <Col md={5}>
                            { imagePreviewUrl ? <Image src={imagePreviewUrl} rounded /> :  
                            <Image src="/images/default-img.png" rounded />
                            }
                        </Col>
                    </Row>
                </div>
            </Container>
            </div>
        )
    }
}

export default connect(null, { insertArticle })(InsertArticle);

