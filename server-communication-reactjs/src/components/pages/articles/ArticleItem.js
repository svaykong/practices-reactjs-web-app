import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { Button, Image, Row, Col, Container } from 'react-bootstrap';
import { connect } from 'react-redux';
import { deleteArticle } from '../../../redux/actions/articleActions';

const ArticleItem = ({ article, deleteArticle }) => {
    const [loading, setLoading] = useState(false);
    const onDelete = () => {
        setLoading(true);        
        deleteArticle(article._id);
        setTimeout(() => {
            alert(`Delete post id: ${article._id} successfully!`);
            setLoading(false);
        }, 1000);
    }
    if(loading) {
        return <h1>Loading...</h1>;
    }
    return (
        <div>
            <Container className="mb-5">
                <Row>
                    <Col md={4} className="mb-3">
                        <Image src={article.image} className="img-fluid" rounded alt={article.title}/>
                    </Col>
                    <Col md={8}>
                        <h2>{article.title}</h2>
                        <p>Create Date : {article.createdAt}</p>
                        <p className= "mb-4">Category : { article.category ? article.category.name : "No Type" }</p>
                        <p>{article.description}</p>
                        <Button variant="dark" as={Link} to={`/articles/view/${article._id}`} >VIEW</Button>
                        <Button className="mx-1" variant="warning" as={Link} to={`/articles/update/${article._id}`}>EDIT</Button>
                        <Button variant="danger" onClick={onDelete}>DELETE</Button>
                    </Col>
                </Row> 
            </Container>
        </div>
    )
}

export default connect(null, { deleteArticle })(ArticleItem);
