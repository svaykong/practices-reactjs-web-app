import Axios from 'axios';

export const getAllCategories = (callBack) => {
    Axios.get('http://110.74.194.125:3535/api/category').then(res => {
        callBack(res);
    }).catch(err => console.log(err));;
};

export const insertCategory = (category, callBack) => {
    Axios.post('http://110.74.194.125:3535/api/category', category).then(res => {
        callBack(res)
    }).catch(err => console.log(err));
}

export const updateCategory = (category, callBack) => {
    Axios.put(`http://110.74.194.125:3535/api/category/${category._id}`, category).then(res => {
        callBack(res)
    }).catch(err => console.log(err));
}

export const deleteCategory = (id, callBack) => {
    Axios.delete(`http://110.74.194.125:3535/api/category/${id}`).then(res => {
        callBack(res)
    }).catch(err => console.log(err));
};

export const getCategoryById = (id, callBack)  => {
    Axios.get(`http://110.74.194.125:3535/api/category${id}`).then(res => {
        callBack(res)
    }).catch(err => console.log(err));
};


