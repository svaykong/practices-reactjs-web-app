import { 
    GET_ARTICLES,  
    INSERT_ARTICLE, 
    GET_ARTICLE,
    DELETE_ARTICLE, 
    UPDATE_ARTICLE, 
    ARTICLE_ERROR, 
    SET_LOADING 
} from '../../actions/actionType';

const initState = {
    articles: null,
    current: null,
    loading: false,
    error: null,
}

export const articleReducer = (state = initState, action) => {
    switch (action.type) {
        case GET_ARTICLES: {
            return { 
                ...state, 
                articles: action.payload,
                loading: false 
            }
        }

        case INSERT_ARTICLE: {
            return {
                ...state,
                articles: [...state.articles, action.payload],
                loading: false
            }
        }

        case GET_ARTICLE: {
            return {
                ...state,
                current: action.payload,
                loading: false
            }
        }

        case DELETE_ARTICLE: {
            return {
                ...state,
                articles: state.articles.filter(article => article._id !== action.payload),
                loading: false
            }
        }

        
        case UPDATE_ARTICLE: {
            return {
                ...state,
                articles: state.articles.map(article => article._id === action.payload.id ? action.payload : article),
            }
        }

        case SET_LOADING: {
            return {
                ...state,
                loading: true
            }
        }

        case ARTICLE_ERROR: {
            return {
                ...state,
                error: action.payload
            }
        }

        default:
            return state
    }
}

export default articleReducer;