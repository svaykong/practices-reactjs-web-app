import { combineReducers } from 'redux'
import articleReducer from './articleReducer/articleReducer';

const reducers = {
    articleReducer: articleReducer,
};

export const rootReducer = combineReducers(reducers);
