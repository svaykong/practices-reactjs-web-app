import React from 'react';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import NavbarLayout from "./components/layouts/NavbarLayout";
import ListArticles from './components/pages/articles/ListArticles';
import InsertArticle from './components/pages/articles/InsertArticle';
import Category from './components/pages/categories/Category';
import ViewArticle from './components/pages/articles/ViewArticle';
import UpdateArticle from './components/pages/articles/UpdateArticle';

const App = () => {
  return (
    <div className="container">
      <Router>
        <NavbarLayout />
          <Switch>
            
            <Route exact path="/" component={ListArticles} />
            
            <Route exact path="/articles" component={ListArticles} />
            <Route exact path="/articles/create" component={InsertArticle} />
            <Route exact path="/articles/view/:id" component={ViewArticle} />
            <Route exact path="/articles/update/:id" component={UpdateArticle} />

            <Route exact path="/category" component={Category} />
            
            <Route path="*" render={() => <h1 className="text-center">404 Pages, NotFound!</h1>}/>

          </Switch>
      </Router>
    </div>
  );
}

export default App;
