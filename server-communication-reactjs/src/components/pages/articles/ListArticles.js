import React, { Component } from 'react'
import { connect } from "react-redux";
import ArticleItem  from './ArticleItem';
import { Link } from 'react-router-dom';
import { getAllArticles } from "../../../redux/actions/articleActions";
import { getAllCategories } from '../../../redux/actions/categoryActions';
import { Button } from 'react-bootstrap';

class ListArticles extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: null
        }
    }

    componentDidMount() {
        const { getAllArticles } = this.props;
        getAllArticles();
        getAllCategories((res) => {
            if(res.status === 200) {
                this.setState({ categories: res.data.data })
            }
        });
    }

    render() {
        const { articles, loading } = this.props.article;
        const { categories } = this.state;
        if(loading || articles === null || categories === null ) {
            return <h1>Loading...</h1>;
        }
        return (
            <div className="py-3">
                <div className="row my-4">
                    <div className="col-lg-5 col-md-12 col-sm-12"><h1>Article Management</h1></div>
                    <div className="col-lg-7 col-md-12 col-sm-12 text-right">
                        <form className="justify-content-end form-inline">
                            <div className="row">
                                <div className="form-group">
                                <label className="form-label">Category</label>
                                <select className="mx-2 custom-select">
                                    <option>All</option>
                                    { 
                                        categories !== null ? 
                                        categories.map(category => <option key={category._id} value={category.name} >{category.name}</option>) : ''}
                                </select>
                                </div>
                            <input
                                placeholder="Search"
                                type="text"
                                className="mr-sm-2 form-control" />
                            <Button variant="outline-success">SEARCH</Button>
                            <Button variant="primary" as={Link} to='/articles/create'  className="mx-2 btn-md">ADD ARTICLE</Button>
                        </div>
                        </form>
                    </div>
                </div>
                { !loading && articles.length === 0 ? (<h3>No data yet...</h3>
                ) : (
                    articles.map(article => <ArticleItem article={article} key={article._id}/>)
                )}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { article: state.articleReducer }
};

export default connect(mapStateToProps, { getAllArticles })(ListArticles);
