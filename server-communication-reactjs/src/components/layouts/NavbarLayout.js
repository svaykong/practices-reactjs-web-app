import React from 'react';
import { Navbar, Nav, Image, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";

export const NavbarLayout = () => {
    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand as={Link} to="/" href="#home"><Image src="/images/KSHRD_logo.png" width="50" alt="kshrd_logo"/> Article Management</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to="/articles" href="#home">Article</Nav.Link>
                        <Nav.Link as={Link} to="/category" href="#link">Category</Nav.Link>
                        <NavDropdown title="Language" id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/EN">English</NavDropdown.Item>
                            <NavDropdown.Item href="#action/KH">Khmer</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
}

export default NavbarLayout;