import React, { Component } from 'react';
import { Form, Row, Button, Table } from 'react-bootstrap';
import { 
    getAllCategories, 
    insertCategory, 
    updateCategory,
    deleteCategory 
} from '../../../redux/actions/categoryActions';

export class Category extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: null,
            loading: true,
            categoryId: "",
            categoryName: "",
            categoryNameError: "",
            isUpdate : false
        }
    }

    componentDidMount() {
        getAllCategories((res) => {
            if(res.status === 200) {
                this.setState({ 
                    loading: false,
                    categories: res.data.data
                });
            }
        });
    }

    onChange = (event) => {
        this.setState({ categoryName: event.target.value });
    }

    onEdit = (category) => {
        this.setState({ categoryId: category._id, categoryName: category.name, isUpdate: true })
    }

    onDelete = (id) => {
        this.setState({ loading: true });
        deleteCategory(id, (res) => {
            alert(res.data.message);
            getAllCategories((res) => {
                if(res.status === 200) {
                    this.setState({ 
                        loading: false,
                        categories: res.data.data
                    });
                }
            });
        });
    }

    validate = () => {
        let categoryNameError = "";
        if(this.state.categoryName === "") {
            categoryNameError = "Please input category name!";
        }
        if(categoryNameError) {
            this.setState({ categoryNameError });
            return false;
        }
        return true;
    }

    onSubmit = (event) => {
        event.preventDefault();
        const isValid = this.validate();
        if(isValid) {
            // add category name
            if(event.target.innerText === "ADD CATEGORIES") {
                const newCategory = {};
                newCategory.name = this.state.categoryName;
                this.setState({ loading: true });
                insertCategory(newCategory, (res) => {
                    alert(res.data.message);
                    getAllCategories((res) => {
                        if(res.status === 200) {
                            this.setState({ 
                                loading: false,
                                categories: res.data.data,
                                categoryNameError: ""
                            });
                        }
                    });
                });
                this.setState({ categoryName: "" });
            }
            // update category name
            else {
                const newCategory = {};
                newCategory._id = this.state.categoryId;
                newCategory.name = this.state.categoryName;
                this.setState({ loading: true });
                updateCategory(newCategory, (res) => {
                    alert(res.data.message);
                    getAllCategories((res) => {
                        if(res.status === 200) {
                            this.setState({ 
                                loading: false,
                                categories: res.data.data,
                                categoryNameError: ""
                            });
                        }
                    });
                });
                this.setState({ categoryId: "", categoryName: "", isUpdate: false });
            }
        }
    }

    handleCancel = (event) => {
        event.preventDefault();
        this.setState({ categoryName: "", isUpdate: false });
    }

    render() {
        const { categoryName, isUpdate, categories, loading, categoryNameError } = this.state;
        if(loading || categories === null ) {
            return <h1>Loading...</h1>;
        }
        return (
            <div>
                <h1>Category</h1>
                <Form>
                    <Row>
                        <Form.Group controlId="formGroupEmail" className="mt-3">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" name="categoryName" onChange={this.onChange} value={categoryName} placeholder="Input Category Name" style={{width : "200px"}} />
                        <h5 className="text-danger">{ categoryNameError ? categoryNameError : ""}</h5>
                    </Form.Group>
                        <div style={{marginTop: '48px'}} className="ml-2">
                            <Button type="submit" variant="dark" className="mr-2" onClick={this.onSubmit}>{ isUpdate ? "SAVE" : "ADD CATEGORIES"}</Button>
                            {
                                isUpdate ? <Button type="submit" variant="primary" onClick={this.handleCancel}>Cancel</Button> : null
                            }
                        </div>
                    </Row>
                </Form>
                <Table style={{textAlign: 'center'}} striped bordered hover>
                <thead >
                    <tr >
                    <th style={{width: '180px'}}>#</th>
                    <th style={{width: '700px'}}>Name</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                { 
                    categories.length > 0 ? 
                    categories.map((category, index) => 
                    <tr key={category._id}>
                        <td>{index += 1}</td>
                        <td>{category.name}</td>
                        <td>
                            <Button type="submit" variant="primary" className="mr-2" onClick={() => this.onEdit(category)}>Edit</Button>
                            <Button type="submit" variant="danger" onClick={() => this.onDelete(category._id)}>Delete</Button>
                        </td>
                    </tr>
                    ) : ''
                }
                </tbody>
            </Table>
            </div>
        );
    }
}

export default Category;
